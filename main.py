# SPDX-FileCopyrightText: 2020 Carl Schwan <carl@carlshwan.eu>
# SPDX-FileCopyrightText: 2021-2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: LGPL-2.0-or-later

from __future__ import annotations

import configparser
import copy
import datetime
import importlib.resources as pkg_resources
import json
import logging
import os
import subprocess

import polib
import requests
from hugoi18n import command_line, generation
from markdown_it import MarkdownIt
from yaml import safe_load, dump


def get_snap_url(common_id):
    req_url = f'http://api.snapcraft.io/v2/snaps/find?fields=store-url&common-id={common_id}'
    headers = {'Snap-Device-Series': '16'}
    try:
        req = requests.get(req_url, headers=headers)
        url = req.json()['results'][0]['snap']['store-url']
    except (requests.exceptions.ConnectionError, ValueError) as err:
        logging.exception(f'{common_id}: {err}')
        url = ''
    except IndexError:
        url = ''
    return url


def gen_id_matches():
    """
    Create or update the app-id-matches.yaml file containing the map from app ids to archlinux names and snap urls.
        Plugins and unmaintained apps are excluded. Fields that have been set are kept based on the assumption that
        once an app name (in archlinux) or url (in snap) is set, it's not changed. There are also many fields that are
        manually entered since they cannot be inferred from app ids.
    - For archlinux names: app short ids are used
    - For snap urls: app ids with and without '.desktop' are used
    It's not like archlinux names and snap urls are updated every day, so this only needs to run once in a while.
    :return:
    """
    file_name = 'app-id-matches.yaml'
    old_matches = {}
    if os.path.isfile(file_name):
        with open(file_name) as f_r:
            old_matches = safe_load(f_r)
    new_matches = {}
    for f in appdata_files:
        app_id = os.path.splitext(f)[0]
        app_short_id = '.'.join(app_id.split('.')[2:])
        if app_short_id in unmaintained_apps or app_short_id.startswith('okular-'):
            continue

        archlinux_done, snap_done = False, False
        new_matches[app_id] = {}
        if app_id in old_matches:
            if old_matches[app_id]['archlinux']['name']:
                new_matches[app_id]['archlinux'] = old_matches[app_id]['archlinux']
                archlinux_done = True
            if old_matches[app_id]['snap']['url']:
                new_matches[app_id]['snap'] = old_matches[app_id]['snap']
                snap_done = True
            if archlinux_done and snap_done:
                continue

        if not archlinux_done:
            archlinux_url = f'https://pkgstats.archlinux.de/api/packages/{app_short_id}'
            res = requests.get(archlinux_url)
            new_matches[app_id]['archlinux'] = {'name': app_short_id if res.json()['popularity'] else ''}
        if not snap_done:
            common_id_desktop = app_id + '.desktop'
            snap_url = get_snap_url(common_id_desktop) or get_snap_url(app_id)
            new_matches[app_id]['snap'] = {'url': snap_url}
    with open(file_name, 'w') as f_w:
        dump(new_matches, f_w, default_flow_style=False, allow_unicode=True)


def fetch_flathub() -> dict:
    def condition_id(id_str: str) -> bool:
        return id_str.startswith('org.kde.') or id_str.startswith('im.kaidan.')

    all_apps = requests.get("https://flathub.org/api/v1/apps").json()
    kde_apps = {app['flatpakAppId']: {
        'downloads': 0,
        'first_date': '',
        'last_date': '',
        'downloads_per_day': 0,
        'popularity': 0
    } for app in all_apps if condition_id(app['flatpakAppId'])}

    today = datetime.date.today()
    year, month = today.year, today.month-1
    if month == 0:
        month = 12
        year -= 1

    stats_path = f'stats/{year}/{month:02}'
    if not os.path.isdir(stats_path):
        if subprocess.call(["wget", "--recursive", "--no-host-directories", "--no-parent", "--reject-regex", "backup",
                            "--accept", "*.json", f'https://flathub.org/{stats_path}/']) != 0:
            return kde_apps

    files = []
    for (dirpath, _, filenames) in os.walk(stats_path):
        files.extend(os.path.join(dirpath, filename) for filename in filenames)
    files.sort()

    for f in files:
        with open(f) as f_json:
            data = json.load(f_json)
            for app_id in data['refs']:
                if app_id in kde_apps:
                    if not kde_apps[app_id]['first_date']:
                        kde_apps[app_id]['first_date'] = data['date']
                    else:
                        kde_apps[app_id]['last_date'] = data['date']
                    arch_sum = sum([value[0] for value in data['refs'][app_id].values()])
                    kde_apps[app_id]['downloads'] += arch_sum

    for app_id in kde_apps:
        if kde_apps[app_id]['downloads'] == 0:
            kde_apps[app_id]['downloads_per_day'] = 0
        else:
            first_date = datetime.datetime.strptime(kde_apps[app_id]['first_date'], '%Y/%m/%d')
            last_date = datetime.datetime.strptime(kde_apps[app_id]['last_date'], '%Y/%m/%d')
            days = (last_date - first_date).days + 1
            kde_apps[app_id]['downloads_per_day'] = kde_apps[app_id]['downloads'] / days
    download_sum_per_day = sum([kde_apps[app_id]['downloads_per_day'] for app_id in kde_apps])
    for app_id in kde_apps:
        kde_apps[app_id]['popularity'] = kde_apps[app_id]['downloads_per_day'] / download_sum_per_day * 100
    return kde_apps


def fetch_archlinux_snap() -> dict:
    with open('app-id-matches.yaml') as f:
        kde_apps = safe_load(f)
        for app_id in kde_apps:
            app_name = kde_apps[app_id]['archlinux']['name']
            if app_name:
                url = f'https://pkgstats.archlinux.de/api/packages/{app_name}'
                res = requests.get(url)
                popularity = res.json()['popularity']
                # print(f'{app_name}: {popularity}')
                kde_apps[app_id]['archlinux']['popularity'] = popularity
        return kde_apps


def prepare_screenshot_data(data: dict):
    data['screenshots'] = []
    data['screenshot_captions'] = []
    data['images'] = []
    for screenshot in data.pop('Screenshots', []):
        data['screenshots'].append({'url': screenshot['source-image']['url']})
        data['screenshot_captions'].append(screenshot.get('caption', {}))
        data['images'].append(screenshot['source-image']['url'])


def prepare_release_data(data: dict):
    data['releases'] = []
    data['release_descriptions'] = []
    latest_stable_release = None
    latest_stable_release_timestamp = -1
    latest_development_release = None
    latest_development_release_timestamp = -1
    for release in data.pop('Releases', []):
        if 'unix-timestamp' in release:
            release['date'] = datetime.date.fromtimestamp(int(release['unix-timestamp'])).isoformat()
        else:
            continue

        if 'url' in release:
            release['url'] = release['url']['details']
        if release['type'] == "stable" and release["unix-timestamp"] > latest_stable_release_timestamp:
            latest_stable_release_timestamp = release["unix-timestamp"]
            latest_stable_release = release
        elif release['type'] == "development" and release["unix-timestamp"] > latest_development_release_timestamp:
            latest_development_release_timestamp = release["unix-timestamp"]
            latest_development_release = release

        data['release_descriptions'].append(release.pop('description', {}))
        data['releases'].append(release)

    if latest_development_release:
        data['latestDevelopmentRelease'] = latest_development_release
    if latest_stable_release:
        data['latestStableRelease'] = latest_stable_release


def prepare_common_data(data: dict, flathub_apps, archlinux_snap, windows_nightly_data):
    app_id = data['ID']
    app_short_id = '.'.join(app_id.split('.')[2:])

    data['KDEProject'] = data.pop('X-KDE-Project', '')
    data['repository'] = data.pop('X-KDE-Repository', '')
    data['appType'] = data.pop('Type')

    if data['KDEProject'].startswith('unmaintained') or app_short_id in unmaintained_apps:
        data['Categories'] = ['unmaintained']
    elif 'Categories' not in data or not data['Categories']:
        if data['appType'] == 'addon':
            data['Categories'] = ['addons'] if not app_short_id.startswith('okular-') else ['office']
        elif app_short_id == 'kitinerary-extractor':
            data['Categories'] = ['office', 'utilities']
        else:
            data['Categories'] = ['unmaintained']
    # otherwise, when data['Categories'] has at least one item, keep it

    data['MainCategory'] = data['Categories'][0]
    if len(data['Categories']) > 1 and '/' in data['repository']:
        repo_category = data['repository'].split('/')[0]
        if repo_category in data['Categories']:
            data['MainCategory'] = repo_category
        elif repo_category == 'pim' and 'office' in data['Categories']:
            data['MainCategory'] = 'office'
    # now 'Categories' is a list of lower-case English names of categories, which can be considered category 'codes';
    #     localized category names are in files in 'content/categories' directory;
    #     'MainCategory' is the first element in 'Categories', i.e. the first category 'code'.

    data['flathub'] = app_id in flathub_apps
    data['snap_url'] = archlinux_snap[app_id]['snap']['url'] if app_id in archlinux_snap else ''

    archlinux_popularity = archlinux_snap[app_id]['archlinux'].get('popularity', 0) if app_id in archlinux_snap else 0
    flathub_popularity = flathub_apps[app_id]['popularity'] if app_id in flathub_apps else 0
    flathub_weight = 0.5
    data['popularity'] = flathub_popularity * flathub_weight + archlinux_popularity * (1 - flathub_weight)

    if 'Icon' in data:
        icon = data.pop('Icon')
        data['icon'] = icon['local'][0]['name']

    prepare_screenshot_data(data)
    prepare_release_data(data)

    for job in windows_nightly_data:
        if data.get('windowsRelease') and data.get('windowsNightly'):
            break
        if data['Name']['en'] in job['name']:
            if 'Release' in job['name']:
                data['windowsRelease'] = job
            elif 'Nightly' in job['name']:
                data['windowsNightly'] = job

    customs = data.pop('Custom', {})
    platforms = set()
    if 'KDE::windows_store' in customs:
        data['windowsStoreLink'] = customs['KDE::windows_store']
        platforms.add("windows")
    if 'KDE::f_droid' in customs:
        data['fDroidLink'] = customs['KDE::f_droid']
        platforms.add("android")
    if 'KDE::google_play' in customs:
        data['googlePlayLink'] = customs['KDE::google_play']
        platforms.add("android")
    data['platforms'] = list(platforms)
    data['forum'] = customs.get('KDE::forum', '')
    data['irc'] = customs.get('KDE::irc', '')
    data['mailinglist'] = customs.get('KDE::mailinglist', '')
    data['matrixChannel'] = customs.get('KDE::matrix', '')
    data['develMailingList'] = customs.get('KDE::mailinglist-devel', '')

    urls = data.pop('Url', {})
    data['homepage'] = urls.get('homepage', '')
    if 'games.kde.org' in data['homepage']:
        data['homepage'] = ''
    data['bugtracker'] = urls.get('bugtracker', '')
    data['help'] = urls.get('help', '')
    data['donation'] = urls.get('donation', '')


def get_localized(data, lang: str) -> str:
    return data.get(lang, data.get('en', ''))


def prepare_localized_data(data, working_lang_codes):
    app_id = data['ID']
    app_short_id = '.'.join(app_id.split('.')[2:])

    name = data.pop('Name')
    summary = data.pop('Summary')
    dev_name = data.pop('DeveloperName', {})
    description = data.pop('Description', {})
    generic_name = data.pop('X-KDE-GenericName')
    screenshots = data.pop('screenshots')
    captions = data.pop('screenshot_captions')
    releases = data.pop('releases')
    release_descriptions = data.pop('release_descriptions')
    # at this point data contains only common fields
    data['localized'] = {}
    for lang in working_lang_codes:
        app_name = get_localized(name, lang)
        app_summary = get_localized(summary, lang)
        localized_props = {'name': app_name,
                           'title': app_name,
                           'summary': app_summary,
                           'description': app_summary,
                           'dev_name': get_localized(dev_name, lang),
                           'GenericName': get_localized(generic_name, lang)}
        if description:
            localized_props['appDescription'] = get_localized(description, lang)
        localized_props['aliases'] = (
                [f'/{app_id}'] +
                [a for c in data['Categories'] for a in [f'/{c}/{app_id}', f'/{c}/{app_short_id}']] if lang == 'en'
                else [f'/{lang}/{app_id}'] +
                     [a for c in data['Categories'] for a in [f'/{lang}/{c}/{app_id}', f'/{lang}/{c}/{app_short_id}']])
        localized_screenshots = copy.deepcopy(screenshots)
        for idx, s in enumerate(localized_screenshots):
            s['caption'] = get_localized(captions[idx], lang)
        localized_props['screenshots'] = localized_screenshots
        localized_releases = copy.deepcopy(releases)
        for idx, r in enumerate(localized_releases):
            r['description'] = get_localized(release_descriptions[idx], lang)
        localized_props['releases'] = localized_releases
        data['localized'][lang] = localized_props


def prepare_app_data():
    flathub_apps = fetch_flathub()
    archlinux_snap = fetch_archlinux_snap()
    windows_nightly_data = requests.get("https://binary-factory.kde.org/view/Windows%2064-bit/api/json").json()["jobs"]

    app_data = []
    app_counts = {}

    for file_name in appdata_files:
        with open(f'static/appdata/{file_name}') as f:
            data = safe_load(f)
        prepare_common_data(data, flathub_apps, archlinux_snap, windows_nightly_data)
        app_data.append(data)

        for lang_code in data['Summary']:
            if lang_code in app_counts:
                app_counts[lang_code] += 1
            else:
                app_counts[lang_code] = 1

    # only process languages with more than 50 translated apps
    working_lang_codes = dict(filter(lambda elem: elem[1] > 50, app_counts.items())).keys()
    for data in app_data:
        prepare_localized_data(data, working_lang_codes)
    return app_data, working_lang_codes


def write_file_frontmatter(path: str, *fms: dict):
    with open(path, 'w') as f:
        f.write('---\n')
        for fm in fms:
            f.write(dump(fm, allow_unicode=True))
        f.write('---\n')


def gen_one_app(data):
    app_short_id = '.'.join(data['ID'].split('.')[2:])
    # index.html
    if app_short_id == 'index':
        app_short_id = 'index-fm'
    localized_data = data.pop('localized', {})
    for lang, lang_data in localized_data.items():
        output_file = f'content/applications/{app_short_id}.{lang}.md'
        write_file_frontmatter(output_file, data, localized_data[lang])


def gen_term_file(taxo, term, hugo_lang_code, tr=None):
    file_path = f'content/{taxo}/{term}/_index.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    content = {
        'title': tr(term.capitalize()) if tr else term.capitalize(),
        'aliases': [f'{lang_prefix}/{term}']
    }
    if term in cats_with_subcats:
        content['layout'] = f'{term}_cat'
    write_file_frontmatter(file_path, content)


def gen_subcat_file(cat, subcat, hugo_lang_code, lang_subcat_name):
    file_path = f'content/categories/{cat}/{subcat}.{hugo_lang_code}.md'
    lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
    content = {
        'title': lang_subcat_name,
        'aliases': [f'{lang_prefix}/{cat}/{subcat}'],
        'layout': f'{cat}_subcat'
    }
    write_file_frontmatter(file_path, content)


def gen_categories(working_lang_codes):
    # subcategory icon names have '-'
    categories = [os.path.splitext(x)[0] for x in os.listdir('static/app-icons/categories') if '-' not in x]
    # we don't need 'settingsmenu'. And 'unmaintained' is handled by 'gen_others'
    to_skip = ['settingsmenu', 'unmaintained']
    categories = [x for x in categories if x not in to_skip]

    # translate category names
    # we can also use desktop files in plasma-workspace/share/desktop-directories
    # but PO messages are grouped by languages, much more convenient to work with
    # than strings in those desktop files
    po_dir = 'pos_cat'
    os.environ['PACKAGE'] = 'plasma-workspace'
    os.environ['FILENAME'] = 'plasma-workspace._desktop_'
    command_line.fetch_langs(working_lang_codes, True, po_dir)

    for cat in categories:
        os.makedirs(f'content/categories/{cat}', exist_ok=True)

    for hugo_lang_code in working_lang_codes:
        strings = {}
        lang_code = generation.revert_lang_code(hugo_lang_code)
        po_path = f'{po_dir}/{lang_code}.po'
        if os.path.isfile(po_path):
            po_file = polib.pofile(po_path)

            def translate(msgid: str) -> str:
                return po_file.find(msgid).msgstr
        else:
            def translate(msgid: str) -> str:
                return msgid

        for cat in categories:
            gen_term_file('categories', cat, hugo_lang_code, translate)
            strings[cat] = {'other': translate(cat.capitalize())}
        for cat in cats_gen_subcat_files:
            for subcat_code, subcat_name in cats_subcats[cat].items():
                lang_subcat_name = translate(subcat_name)
                gen_subcat_file(cat, subcat_code, hugo_lang_code, lang_subcat_name)
                strings[f'{cat}-{subcat_code}'] = {'other': lang_subcat_name}
        for cat in cats_not_gen_subcat_files:
            for subcat_code, subcat_name in cats_subcats[cat].items():
                strings[f'{cat}-{subcat_code}'] = {'other': translate(subcat_name)}

        i18n_path = f'i18n/{hugo_lang_code}.yaml'
        with open(i18n_path, 'a') as f_i18n:
            f_i18n.write(dump(strings, default_flow_style=False, allow_unicode=True))
    # shutil.rmtree(po_dir)


def get_subcat_names(cat: str) -> dict[str, str]:
    if cat == 'education':
        cat = 'edu'
    subcat_desktop_files = [x for x in os.listdir(desktopDirPath) if x.startswith(f'kf5-{cat}-')]
    subcat_pairs = {}
    for subcat_desktop_file in subcat_desktop_files:
        subcat_code = subcat_desktop_file.split('.')[0].split('-')[2]
        with open(f'{desktopDirPath}/{subcat_desktop_file}') as f_desktop:
            cp = configparser.ConfigParser()
            cp.read_file(f_desktop)
            section = cp['Desktop Entry']
            subcat_pairs[subcat_code] = section['Name']
    return subcat_pairs


def gen_index_file(section, hugo_lang_code, tr=None):
    if section == 'home':
        file_path = f'content/_index.{hugo_lang_code}.md'
        lang_prefix = '' if hugo_lang_code == 'en' else f'/{hugo_lang_code}'
        title = 'KDE Applications'
        content = {
            'aliases': [f'{lang_prefix}/applications'],
            'title': tr(title) if tr else title
        }
    else:
        file_path = f'content/{section}/_index.{hugo_lang_code}.md'
        title = 'Other platforms' if section == 'platforms' else section.capitalize()
        content = {
            'menu': {'main': {'weight': 2 if section == 'categories' else 3}},
            'title': tr(title) if tr else title
        }
    write_file_frontmatter(file_path, content)


def gen_platforms(hugo_lang_code, tr=None):
    gen_index_file('platforms', hugo_lang_code, tr)

    for platform in other_platforms:
        gen_term_file('platforms', platform, hugo_lang_code, tr)


def gen_others(working_lang_codes):
    en_strings, configs, original_configs, src_data = generation.read_sources()

    lang_config = list(configs['languages'].keys())
    with pkg_resources.open_text('hugoi18n.resources', 'languages.yaml') as f_langs:
        lang_names = safe_load(f_langs)
    for config_lang_code in lang_config:
        if config_lang_code not in working_lang_codes:
            del configs['languages'][config_lang_code]

    for platform in other_platforms:
        os.makedirs(f'content/platforms/{platform}', exist_ok=True)
    os.makedirs(f'content/categories/unmaintained', exist_ok=True)
    mdi = MarkdownIt(renderer_cls=generation.LocalizedMdRenderer).enable('table')
    for hugo_lang_code in working_lang_codes:
        if hugo_lang_code in ['en']:
            gen_platforms(hugo_lang_code)
            gen_index_file('categories', hugo_lang_code)
            gen_index_file('home', hugo_lang_code)
            gen_term_file('categories', 'unmaintained', hugo_lang_code)
        else:
            lang_code = generation.revert_lang_code(hugo_lang_code)
            os.environ["LANGUAGE"] = lang_code
            tr = generation.gettext_func(os.environ['FILENAME'])

            generation.generate_strings(en_strings, hugo_lang_code, tr)
            generation.generate_languages(configs, lang_names, lang_code, hugo_lang_code)
            generation.generate_menu(configs, hugo_lang_code, tr)
            generation.generate_description(configs, hugo_lang_code, tr)
            generation.generate_title(configs, hugo_lang_code, tr)
            generation.generate_data_files(configs['i18n']['excludedDataKeys'].split(),
                                           src_data, hugo_lang_code, tr, mdi)

            gen_platforms(hugo_lang_code, tr)
            gen_index_file('categories', hugo_lang_code, tr)
            gen_index_file('home', hugo_lang_code, tr)
            gen_term_file('categories', 'unmaintained', hugo_lang_code, tr)

    generation.write_target(configs, original_configs)


if __name__ == "__main__":
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s: %(message)s', level=level)

    subprocess.run(['python3', 'main.py'], cwd='appdata-extractor/')

    with open('appdata-extractor/unmaintained.json') as f_u:
        unmaintained_apps = json.load(f_u)

    plasma_addons = ['org.kde.plasma.katesessions', 'org.kde.plasma.printmanager', 'org.kde.plasma.wacomtablet',
                     'org.kde.plasma.worldclock']
    non_apps = ['org.kde.atcore', 'org.kde.kdialog', 'org.kde.khelpcenter', 'org.kde.sieveeditor']
    to_ignore = ['.gitignore'] + [n + '.json' for n in plasma_addons + non_apps]
    appdata_files = set(os.listdir('static/appdata')) - set(to_ignore)

    # gen_id_matches()  # this only needs to run once in a while or only when necessary

    all_app_data, working_langs = prepare_app_data()

    os.makedirs('content/applications', exist_ok=True)
    for one_app in all_app_data:
        gen_one_app(one_app)
    logging.info('Applications done')

    other_platforms = ['android', 'windows']
    # create directory before we can generate index files in 'gen_others'
    os.makedirs('content/categories', exist_ok=True)
    # we need to have cats_with_subcats before it can be used in gen_term_file inside gen_others
    plasmaworkspaceDirPath = 'appdata-extractor/plasma-workspace'
    # plasmaworkspaceDirPath = '/usr'
    desktopDirPath = f'{plasmaworkspaceDirPath}/share/desktop-directories'
    # key: category code
    # value: dict of (subcategory code, subcategory name)
    cats_subcats = {}
    # categories with subcategories that we will process
    cats_with_subcats = ['games']
    # categories not to generate subcategory files for
    cats_not_gen_subcat_files = ['games']
    # categories to generate subcategory files for
    cats_gen_subcat_files = (x for x in cats_with_subcats if x not in cats_not_gen_subcat_files)

    for category in cats_with_subcats:
        subcat_names = get_subcat_names(category)
        cats_subcats[category] = subcat_names
        # gen subcat list data
        with open(f'data/{category}_subcats.yaml', 'w') as f_subcats:
            f_subcats.write(dump(list(subcat_names.keys())))

    # translate strings, title, description, and menu
    app_po_dir = 'pos_app'
    os.environ['PACKAGE'] = 'websites-apps-kde-org'
    os.environ['FILENAME'] = 'kde-org-applications'
    command_line.fetch_langs(working_langs, True, app_po_dir)
    command_line.compile_po_in(app_po_dir)
    gen_others(working_langs)
    # shutil.rmtree('locale')
    # shutil.rmtree(po_dir)
    logging.info('L10n done')

    # categories and subcategories generation: this needs to be after i18n files have been generated
    gen_categories(working_langs)
    logging.info('Categories done')
