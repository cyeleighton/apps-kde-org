# KDE Apps website
[![Build Status](https://binary-factory.kde.org/buildStatus/icon?job=Website_apps-kde-org)](https://binary-factory.kde.org/job/Website_apps-kde-org/)

This is a Hugo-based website displaying all the applications made by the KDE community.

As a (Hu)Go module, it requires both Hugo and Go to work.

## Development
- Read about the shared theme at [kde-hugo wiki](https://invent.kde.org/websites/aether-sass/-/wikis/Hugo);
- Read about the extractor [here](https://invent.kde.org/websites/kde-org-applications-extractor) and [here](https://community.kde.org/KDE.org/applications);
- Read about and install [hugo-i18n](https://invent.kde.org/websites/hugo-i18n);
- Run commands:
```sh
git clone git@invent.kde.org:websites/apps-kde-org
cd apps-kde-org
python3 main.py
hugo server
```
- Open http://localhost:1313

## Maintainer
Jonathan Riddell and Carl Schwan. Contact us in #kde-www or #kde-devel :)

## License
This project is licensed under AGPL 3 or later. Individual files can be licensed under another compatible license.
