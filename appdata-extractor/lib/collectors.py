# SPDX-FileCopyrightText: 2017-2018 Harald Sitter <sitter@kde.org>
# SPDX-FileCopyrightText: 2022 Phu Hung Nguyen <phu.nguyen@kdemail.net>
# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

import glob
import json
import logging
import os

from hugoi18n.generation import convert_lang_code

from . import category
from .appdata import AppData
from .icon_fetcher import IconFetcher
from .kde_project import Project
from .xdg.desktop import ApplicationDesktopLoader, Desktop
from .xdg.icon import IconTheme


class CollectorException(Exception):
    pass


class AppstreamCollector:
    APPLICATION_TYPES = ['desktop', 'desktop-application', 'console-application',
                         'web-application', 'addon']
    with open('unmaintained.json') as f_u:
        _unmaintained = json.load(f_u)

    @staticmethod
    def _lang_code_func(lang_code):
        return 'en' if lang_code == 'C' else convert_lang_code(lang_code)

    def __init__(self, directory, path, project: Project, theme):
        self.directory = directory
        self.path = path
        self.project = project
        self.appdata = AppData(self.path).read_serialize(self._lang_code_func)
        # '.desktop' part is not important anymore. We drop it anywhere we can
        self.appid = self.appdata['ID'].replace('.desktop', '')
        self.appdata['ID'] = self.appid
        self.icon_theme = theme
        self.desktop_file = None

    def xdg_data_dir(self):
        return f'{self.directory}/share'

    def get_desktop_file(self):
        if not self.desktop_file:
            # Special case: kdeconnect
            appid = self.appid + '.app' if self.appid == 'org.kde.kdeconnect' else self.appid
            self.desktop_file = \
                ApplicationDesktopLoader(appid, [f'{self.xdg_data_dir()}/applications', 'appdata-unmaintained']) \
                .find()
        return self.desktop_file

    def theme(self):
        if not self.icon_theme:
            self.icon_theme = IconTheme('breeze', [f'{self.xdg_data_dir()}/icons'])
        return self.icon_theme

    def is_desktop_app(self):
        # is type desktop or desktop-application
        return self.appdata['Type'].startswith('desktop')

    # FIXME: overridden for git crawling
    def grab_icon(self):
        if not self.is_desktop_app():
            if 'Icon' in self.appdata and 'stock' in self.appdata['Icon']:
                icon_name = self.appdata['Icon']['stock']
            else:  # addon for now
                icon_name = self.appdata.get('Icon', 'folder-build')
        elif os.path.basename(self.project.id) in type(self)._unmaintained:
            icon_name = 'planetkde'  # a generic icon
        else:
            icon_name = self.get_desktop_file().icon
        return None if not icon_name else IconFetcher(icon_name, self.theme()).extend_appdata(self.appdata, self.appid)

    def grab_categories(self):
        categories = [] if 'Categories' not in self.appdata else self.appdata['Categories']
        # Special cases: khelpcenter
        if self.appid in ['org.kde.khelpcenter']:
            categories = ['System']
        # If the categories are defined in both desktop and appdata files, combine them
        if self.is_desktop_app():
            categories += self.get_desktop_file().categories
            if not categories:
                raise CollectorException(f'{self.appid} has no category')
        # Make sure to filter out all !main categories.
        subcategories = list(set(categories) & set(category.MAIN_SUBCATEGORIES))
        self.appdata['subcategories'] = sorted([category.to_code(x) for x in subcategories])
        categories = list(set(categories) & set(category.MAIN_CATEGORIES))
        self.appdata['Categories'] = sorted([category.to_code(x) for x in categories])

    def grab_generic_name(self):
        generic_name = {}
        if self.is_desktop_app():
            generic_name = self.get_desktop_file().localized('GenericName', self._lang_code_func)
        if not generic_name:
            generic_name = self.appdata['Summary']
        self.appdata['X-KDE-GenericName'] = generic_name

    def grab_project(self):
        self.appdata['X-KDE-Project'] = self.project.id
        self.appdata['X-KDE-Repository'] = self.project.repo

    def grab(self):
        if self.appdata.get('Type', 'generic') not in self.APPLICATION_TYPES:
            logging.warning(f'{self.appid} is not an application')
            return False
        # addons have no desktop files
        if self.is_desktop_app():
            if not self.get_desktop_file():
                raise CollectorException(f'No desktop file for {self.appid}')
            if not self.get_desktop_file().show_in('KDE'):
                raise CollectorException(f'Desktop file for {self.appid} is not meant for display')

        logging.info(f'Now processing ID: {self.appid}')
        # thumbnails are not used anymore
        self.grab_icon()
        self.grab_categories()
        self.grab_generic_name()
        self.grab_project()

        with open(f"../static/appdata/{self.appid}.json", 'w') as f_json:
            json.dump(self.appdata, f_json, ensure_ascii=False, indent=2)

        # FIXME: we should put EVERYTHING into a well defined tree in a tmpdir,
        #   then move it into place in ONE place. so we can easily change where
        #   stuff ends up in the end and know where it is while we are working on
        #   the data
        return True

    @classmethod
    def grab_data(cls, directory, project: Project, theme=None):
        # FIXME: the return value is no good. we need to differentiate:
        #    found no appdata from no good appdata from good appdata
        #    former would need subsequent collectors run, latter simply means the
        #    project contains nothing worthwhile
        if os.path.basename(project.id) in cls._unmaintained:
            k_id = f'org.kde.{os.path.basename(project.id)}'
            # to prevent scripty from processing appdata files in 'appdata-unmaintained' folder,
            #  use the extension '.appdata' instead of '.appdata.xml'
            dirs = glob.glob(f'appdata-unmaintained/{k_id}.appdata')
        else:
            # metainfo.xml is the right extension to use unless you're an
            # app and don't want to use that one in which case use appdata.xml
            dirs = glob.glob(f'{directory}/**/**.appdata.xml', recursive=True)
            dirs += glob.glob(f'{directory}/**/**.metainfo.xml', recursive=True)
        for d in dirs:
            if 'org.example' in d:
                dirs.remove(d)
        any_good = False
        for path in dirs:
            logging.info(f'Grabbing {path}')
            try:
                good = cls(directory, path, project, theme).grab()
                if not any_good:
                    any_good = good
            except CollectorException as e:
                logging.warning(e)
        return any_good


class GitAppstreamCollector(AppstreamCollector):
    def xdg_data_dir(self):
        return f'{os.getcwd()}/breeze-icons/share'

    # FIXME: deferring to appstream via xdg_data_dir
    # def grab_icon
    #   raise 'not implemented'
    #   # a) should look in breeze-icon unpack via theme
    #   # b) should try to find in tree?
    #   # c) maybe an override system?
    # end

    def get_desktop_file(self):
        if self.desktop_file:
            return self.desktop_file
        if os.path.basename(self.project.id) in type(self)._unmaintained:
            k_id = f'org.kde.{os.path.basename(self.project.id)}'
            files = glob.glob(f'appdata-unmaintained/{k_id}.deskto')
        else:
            files = glob.glob(f'{self.directory}/**/{self.appid}.desktop', recursive=True)
            files += glob.glob(f'{self.directory}/**/{self.appid}.desktop.in', recursive=True)
        for f in files:
            if 'snap/setup' in f or 'snap/gui' in f or 'APPNAMELC' in f or 'org.example' in f:
                files.remove(f)
        if len(files) != 1:
            raise Exception(f'{self.appid}: {repr(files)}')
        logging.info(f'Found desktop at {files[0]}')
        self.desktop_file = Desktop(files[0])
        return self.desktop_file
