<?xml version="1.0" encoding="utf-8"?>
<component type="desktop">
  <id>org.kde.kst</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-2.0+</project_license>
  <name>Kst</name>
  <name xml:lang="nl">Kst</name>
  <name xml:lang="uk">Kst</name>
  <summary>Real-time large-dataset viewing and plotting tool</summary>
  <summary xml:lang="uk">Інструмент для перегляду і візуалізації у режимі реального часу великих наборів даних</summary>
  <description>
    <p>
    Kst is the fastest real-time large-dataset viewing and plotting tool available (you may be interested in some benchmarks) and has built-in data analysis functionality.
    </p>
    <p xml:lang="uk">Kst — найшвидший інструмент для перегляду і креслення у режимі реального часу великих обсягів даних (ви можете перевірити це за допомогою тестів). Програма має вбудовані засоби аналізу.</p>
    <p>Features</p>
    <p xml:lang="nl">Mogelijkheden</p>
    <p xml:lang="uk">Можливості</p>
    <ul>
      <li>Robust plotting of live "streaming" data</li>
      <li xml:lang="uk">Надійне креслення інтерактивних потокових даних</li>
      <li>Powerful keyboard and mouse plot manipulation</li>
      <li xml:lang="uk">Потужні можливості з керування кресленням за допомогою миші та клавіатури</li>
      <li>Powerful plugins and extensions support</li>
      <li xml:lang="uk">Підтримка потужних додатків та розширень</li>
      <li>Large selection of built-in plotting and data manipulation functions, such as histograms, equations, and power spectra</li>
      <li xml:lang="uk">Широкий вибір вбудованих функцій для керування кресленням і даними, зокрема гістограми, рівняння та спектральний аналіз</li>
      <li>A number of unique tools which dramatically improve efficiency, such as the "Data Wizard" for fast and easy data import, the "Edit Multiple" mode to bulk-edit most objects, or the "Change Data File" tool to compare results from different experiments</li>
      <li xml:lang="uk">Декілька унікальних інструментів, які значно удосконалюють ефективність, зокрема «Майстер даних» для швидкого та простого імпортування даних, режим «Мультиредагування» для пакетного редагування більшості об'єктів або інструмент «Змінити файл даних» для порівняння результатів різних експериментів.</li>
      <li>Color mapping and contour mapping capabilities for three-dimensional data, as well as matrix and image support</li>
      <li xml:lang="uk">Можливості прив'язки кольорів та контурів для тривимірних даних, а також підтримка роботи з матрицями та зображеннями</li>
      <li>Monitoring of events and notifications support</li>
      <li xml:lang="uk">Стеження за подіями і підтримка сповіщень</li>
      <li>Built-in filtering and curve fitting capabilities</li>
      <li xml:lang="uk">Вбудовані можливості фільтрування та апроксимації кривими</li>
      <li>Convenient command-line interface</li>
      <li xml:lang="uk">Зручний інтерфейс командного рядка</li>
      <li>Powerful graphical user interface with non-modal dialogs for an optimized workflow</li>
      <li xml:lang="uk">Потужний графічний інтерфейс користувача із немодальними вікнами для оптимізації робочого процесу</li>
      <li>Support for several popular data formats</li>
      <li xml:lang="uk">Підтримка декількох популярних форматів запису даних</li>
      <li>Multiple tabs</li>
      <li xml:lang="nl">Meerdere tabbladen</li>
      <li xml:lang="uk">Вкладки</li>
      <li>Extended annotation objects similar to vector graphics applications</li>
      <li xml:lang="uk">Розширені можливості із анотування об'єктів, подібні до програм для роботи з векторною графікою</li>
      <li>High-quality export to bitmap or vector formats</li>
      <li xml:lang="uk">Високоякісний експорт до растрових та векторних форматів зберігання даних</li>
      <li>Fully scriptable in python (Beta available under Linux)</li>
      <li xml:lang="uk">Можливість керування програмою за допомогою скриптів мовою python (тестова версія доступна у Linux)</li>
    </ul>
  </description>
  <url type="homepage">http://www.kde.org</url>
  <url type="donation">https://www.kde.org/community/donations/?app=kst&amp;source=appdata</url>
  <project_group>KDE</project_group>
  <provides>
    <binary>kst</binary>
  </provides>
  <launchable type="desktop-id">org.kde.kst</launchable>
</component>
